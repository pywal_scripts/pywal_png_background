# pywal_png_background

Create a png background from xrdb, json pywal or given color to make an underline for urxvt.
I made this tool to automate changing urxvt background after pywal create a colorscheme

![pixmap](pixmap.png "Pixmap")

## Example

![example](example.png "Example")

## Usage example

They are an example loading the last generated pywal in apply_png.sh

Create a PNG with

```bash
./create_png.py -i /path/to/pywal.json -w 6 -H 28 -o /where/to/save/pixmap
```

You must:

- Adjust height according to your font terminal size to keep a beautiful effect

- Change paths to wanted ones

Apply changes to all open terminals

```bash
pixmap="/path/to/pixmap.png"

sequence="\033]20;$pixmap;style=tiled\033\\"

for f in /dev/pts/[0-9]*
do
printf $sequence > $f
done
```

Make this persistent in Xresources

```bash
pixmap="/path/to/pixmap.png"

echo "URxvt*backgroundPixmap: $pixmap;style=tiled" >> ~/.Xresources
```

There are other options available:

```bash
# help
usage: create_png.py [-h] -w N -H N [-d N] [-i N] [-b N] [-f N] [-x]
                     [-D] -o OUTPUT

Create a PNG background for URxvt

optional arguments:
  -h, --help            show this help message and exit
  -w N, --width N       PNG width
  -H N, --height N      PNG height
  -d N, --dot N         Space between dots
  -i N, --input N       Path to pywal json scheme to apply
  -b N, --background N  Force background color to apply
  -f N, --foreground N  Force foreground color to apply
  -x, --xrdb            use colors returned by xrdb
  -D, --delete          Delete last generated pywal json
  -o OUTPUT, --output OUTPUT
                        Directory where to save generated PNG

```

## TODO

- [ ] Handle errors and bad combination
- [ ] Improve dot spaces
