#!/bin/bash

lastPywal="$(ls -Art ~/.cache/wal/schemes| tail -n 1)"

bg_color="$(jq -r '.special.background' ~/.cache/wal/schemes/"$lastPywal")"
fg_color="$(jq -r '.special.foreground' ~/.cache/wal/schemes/"$lastPywal")"

./create_png.py -x -b $bg_color -f $fg_color -w 6 -H 28 -o ~/pixmap

pixmap="~/pixmap.png"

echo "URxvt*backgroundPixmap: $pixmap;style=tiled" >> ~/.Xresources

sequence="\033]20;$pixmap;style=tiled\033\\"

for f in /dev/pts/[0-9]*
do
printf $sequence > $f
done

# remove used pywal files
rm -f ~/.cache/wal/schemes/$lastPywal